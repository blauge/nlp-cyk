# Algorithme Cocke–Younger–Kasami

Visualisation interactive de l’algorithme CYK qui permet d’analyser rapidement les phrases issues de grammaires en forme normée de Chomsky.

[Voir le site »](https://matteo.delab.re/projects/nlp-cyk/)
