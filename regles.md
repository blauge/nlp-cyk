# Étiquettes grammaticales

## Catégories initiales

Déterminants DET
Substantifs NOMC, NOMP
Pronoms PRNSUJ, PRNOBJ, PRNREL, PRNPNM
Verbes VRB, AUX, PPAS
Modifieurs ADJ, ATT, ADV
Prépositions PREP, DETPREP
Conjonctions COOR, SUB

## Catégories ajoutées

NEGG : partie gauche des formes négatives ("_ne_ pas")
NEGD : partie droite des formes négatives ("ne _pas_", "ne _plus_", "ne _jamais_") [forclusif]

## Catégories des mots demandés

le := DET, PRNOBJ
les := DET, PRNOBJ
l' := DET, PRNOBJ
bien := NOMC, ADV, ADJ
biens := NOM
mal := ADJ, ADV, NOMC
maux := ADJ, NOMC
fait := NOMC, ADJ, VRB, PPAS
faire := VBR, NOMC
que := SUB, PRNREL, PRNSUJ
qu' := SUB, PRNREL, PRNSUJ
je := PRNSUJ
j' := PRNSUJ
il := PRNSUJ
est := NOMC, ADJ, VRB
a := VRB, AUX
ai := VRB, AUX
ait := VRB, AUX
peu := NOMC, ADV
de := PREP, DET
des := DETPREP, DET
d' := DETPREP, DET
du := DETPREP, DET
gens := NOMC
ont := VRB, AUX
fait := PPAS
un := DET, ADJ
à := PREP
ne := NEGG
pas := NOMC, NEGD
ce := DET
autres := NOMC, ADJ
autre := DET, NOMC, ADJ
font := NOMC, VRB
"bien que" := SUB
"bien qu'" := SUB
"ce que" := PRNREL

# Étiquettes syntaxiques

## Phrase

PH -> PROP PONCTFIN

## Proposition

PROP -> GN VRB
PROP -> GN GV
PROP -> PRNSUJ VRB
PROP -> PRNSUJ GV
PROP -> COMPLG PROP
PROP -> PROP COMPLD

## Propositions subordonnées

PROPSUB -> SUB PROP
PROPSUB -> PRNREL VRB
PROPSUB -> PRNREL GV
PROPREL -> PRNREL PROP

## Groupe nominal

GN -> DET NOMC
GN -> LOCADV NOMC
GN -> GN PROPSUB

## Groupe verbal

### Forme négative

GV_NEG -> NEGG VRB
GV_NEG -> NEGG GV
GV -> GV_NEG NEGD

### Passé composé

#### Forme directe

GV -> AUX PPAS

#### Forme contenant des adverbes

GV_AUX -> PRNOBJ AUX
GV_AUX -> AUX ADV
GV_AUX -> GV_AUX ADV
GV -> GV_AUX PPAS

#### Forme négative dans le passé composé

GV_NEG_AUX -> NEGG AUX
GV_NEG_AUX -> NEGG GV_AUX
GV_AUX -> GV_NEG_AUX NEGD

### Complément du verbe

#### Complément d’objet direct

GV -> GV GN
GV -> VRB GN
GV -> PRNOBJ GV
GV -> PRNOBJ VRB

#### Complément d’objet indirect

GV -> GV COI
GV -> VRB COI

COI -> PREP VRB
COI -> PREP GV
COI -> PREP GN

#### Adverbe

GV -> GV ADV
GV -> VRB ADV

#### Proposition

GV -> GV PROPSUB
GV -> VRB PROPSUB
GV -> GV PROPREL
GV -> VRB PROPREL

## Complément de phrase

COMPLG -> GN PONCTCOOR
COMPLG -> PROPSUB PONCTCOOR
COMPLD -> PONCTCOOR GN
COMPLD -> PONCTCOOR PROPSUB

## Locutions

LOCADV -> ADV DETPREP
LOCADV -> ADV PREP
DETADV -> DET ADV
LOCADV -> DETADV DETPREP
LOCADV -> DETADV PREP
