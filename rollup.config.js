import replace from 'rollup-plugin-replace';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';

export default {
    input: 'src/index.js',
    output: {
        file: 'build/index.js',
        format: 'iife',
        name: 'cyk',
        strict: false,
    },
    plugins: [
        babel({
            exclude: 'node_modules/**',
            presets: [
                "@babel/preset-env",
                "@babel/preset-react"
            ],
        }),
        replace({
            'process.env.NODE_ENV': JSON.stringify('development'),
        }),
        resolve({
            browser: true,
        }),
        commonjs(),
    ],
};

