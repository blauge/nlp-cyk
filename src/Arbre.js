import React from "react";
import ReactDOM from "react-dom";
import Tree from "react-d3-tree";

// Nom de l’axiome de la grammaire
const axiom = "PH";

/**
 * Génère tous les arbres de syntaxe possibles pour la phrase d’origine à partir
 * d’une matrice obtenue par l’algorithme CYK. Si la phrase n’est pas reconnue,
 * renvoie un tableau vide.
 *
 * @param mots Phrase d’origine.
 * @param matrice Matrice générée par CYK.
 * @return Liste d’arbres de syntaxe possibles.
 */
const créerArbres = (mots, matrice) =>
{
    const n = mots.length;

    // Cas où la phrase est vide ou invalide
    if (n === 0 || !matrice[n - 1][0].has(axiom))
    {
        return [];
    }

    /**
     * Génère l’arbre issu d’une case donnée de façon récursive.
     *
     * @param profondeur Profondeur courante de l’arbre.
     * @param i Ligne initiale.
     * @param j Colonne initiale.
     * @param production Production recherchée dans la case de départ.
     * @return Fragment d’arbre issu de la case initiale.
     */
    const rec = (profondeur, i, j, production) =>
    {
        if (i === 0)
        {
            return [{
                name: production,
                children: [{
                    name: mots[j],
                    children: [],
                }],
            }];
        }
        else
        {
            return matrice[i][j].get(production).map(({
                k,
                production: [prod_gauche, prod_droite]
            }) => {
                const arbres_gauche = rec(profondeur + 1, k, j, prod_gauche);
                const arbres_droite = rec(profondeur + 1, i - k - 1, j + k + 1, prod_droite);
                const result = [];

                for (let gauche_idx = 0; gauche_idx < arbres_gauche.length; ++gauche_idx)
                {
                    for (let droite_idx = 0; droite_idx < arbres_droite.length; ++droite_idx)
                    {
                        result.push({
                            name: production,
                            children: [
                                arbres_gauche[gauche_idx],
                                arbres_droite[droite_idx],
                            ],
                        });
                    }
                }

                return result;
            }).reduce(
                (acc, val) => acc.concat(val),
                []
            );
        }
    };

    return rec(0, n - 1, 0, axiom);
}

/**
 * Teste l’égalité de deux tableaux.
 *
 * @param tab1 Premier tableau.
 * @param tab2 Second tableau.
 * @return Vrai si et seulement si les deux tableaux sont égaux.
 */
const arrayEquals = (tab1, tab2) =>
{
    if (!Array.isArray(tab1) || !Array.isArray(tab2))
    {
        return false;
    }

    if (tab1.length !== tab2.length)
    {
        return false;
    }

    for (let i = 0; i < tab1.length; ++i)
    {
        if (tab1[i] !== tab2[i])
        {
            return false;
        }
    }

    return true;
};

class Arbre extends React.Component
{
    constructor(props)
    {
        super(props);
        const {mots, matrice} = props;

        this.state = {
            // Phrase dont l’arbre est actuellement affiché
            mots,

            // Indice de l’arbre de syntaxe actuellement affiché
            current: 0,

            // Liste des arbres de syntaxe possibles
            arbres: créerArbres(mots, matrice),
        };
    }

    static getDerivedStateFromProps(props, state)
    {
        if (!arrayEquals(state.mots, props.mots))
        {
            // Recalcul des arbres lorsque la phrase change
            const {mots, matrice} = props;
            return {
                mots,
                current: 0,
                arbres: créerArbres(mots, matrice),
            };
        }

        return null;
    }

    /**
     * Passe à l’arbre suivant.
     */
    next()
    {
        this.setState((state, props) => {
            const {current, arbres} = state;

            if (current < arbres.length - 1)
            {
                return { current: current + 1 };
            }

            return {};
        });
    }

    /**
     * Passe à l’arbre précédent.
     */
    prev()
    {
        this.setState((state, props) => {
            const {current} = state;

            if (current > 0)
            {
                return { current: current - 1 };
            }

            return {};
        });
    }

    render()
    {
        const {current, arbres} = this.state;

        if (arbres.length === 0)
        {
            return <p className="invalide"><span>Phrase invalide.</span></p>;
        }

        const arbre = arbres[current];

        return (
            <div>
                <p className="control">
                    <span
                        title="Passer à l’arbre précédent"
                        onClick={() => this.prev()}
                        className={[
                            "button",
                            current === 0 ? "" : "active",
                        ].join(" ")}
                    >«</span>
                    <span className="status">
                        {current + 1}/{arbres.length}
                    </span>
                    <span
                        title="Passer à l’arbre suivant"
                        onClick={() => this.next()}
                        className={[
                            "button",
                            current === arbres.length - 1 ? "" : "active",
                        ].join(" ")}
                    >»</span>
                </p>

                <Tree
                    data={arbre}
                    orientation="vertical"
                    separation={{siblings: 1, nonSiblings: 1}}
                    translate={{x: 720, y: 100}}
                />
            </div>
        );
    }
}

export default Arbre;
