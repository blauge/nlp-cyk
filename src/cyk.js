const append = (map, key, value) =>
{
    if (map.has(key))
    {
        map.get(key).push(value);
    }
    else
    {
        map.set(key, [value]);
    }
};

const cyk = (phrase, grammaire) =>
{
    const n = phrase.length;
    const matrice = Array.from(
        new Array(n),
        (_, indice) => Array.from(
            new Array(n - indice),
            () => new Map
        )
    );

    for (let j = 0; j < n; ++j)
    {
        for (let [rule_index, [cat_grammaticale, production]] of grammaire.entries())
        {
            if (production.length === 1 && production[0] === phrase[j])
            {
                append(matrice[0][j], cat_grammaticale, {rule_index});
            }
        }
    }

    for (let i = 1; i < n; ++i)
    {
        for (let j = 0; j < n - i; ++j)
        {
            for (let k = 0; k < i; ++k)
            {
                for (let [rule_index, [cat_syntaxique, production]] of grammaire.entries())
                {
                    if (production.length === 2)
                    {
                        const [prod_gauche, prod_droite] = production;

                        if (matrice[k][j].has(prod_gauche)
                            && matrice[i - k - 1][j + k + 1].has(prod_droite))
                        {
                            append(matrice[i][j], cat_syntaxique, {k, production, rule_index});
                        }
                    }
                }
            }
        }
    }

    return matrice;
};

export default cyk;
