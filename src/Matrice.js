import React from "react";
import ReactDOM from "react-dom";

class Matrice extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            // Cellule et production actuellement survolée
            hovered: [],
        };
    }

    enter(i, j, data)
    {
        const {grammaire} = this.props;
        const [_, production] = grammaire[data.rule_index];

        if (production.length === 1 || data.k === undefined)
        {
            // N’est pas une règle composée
            return;
        }

        const {k} = data;
        const [prod_gauche, prod_droite] = production;

        this.setState({
            hovered: [
                {i: k, j, production: prod_gauche},
                {i: i - k - 1, j: j + k + 1, production: prod_droite},
            ],
        });
    }

    leave()
    {
        this.setState({
            hovered: [],
        });
    }

    render()
    {
        const {mots, matrice} = this.props;
        const {hovered} = this.state;

        if (mots.length === 0)
        {
            return <p className="invalide"><span>Matrice vide.</span></p>;
        }

        return (
            <div>
                <p>
                    <strong>{"Aide : "}</strong>
                    Survolez une règle de la matrice pour surligner ses productions.
                </p>

                <table className="matrice">
                <tbody>
                    {/* En-tête : liste des mots de la phrase */}
                    <tr>
                        {mots.map((mot, i) =>
                            <td key={i}>{mot}</td>
                        )}
                    </tr>
                    {/* Corps : résultat de l’algorithme */}
                    {matrice.map((ligne, i) =>
                        <tr key={i}>
                            {ligne.map((cell, j) =>
                                <td key={j}>
                                    <ul>
                                        {Array.from(cell.entries()).map(([production, content]) =>
                                            content.map((data) =>
                                                <li
                                                    key={data.rule_index}
                                                    onMouseEnter={() => this.enter(i, j, data)}
                                                    onMouseLeave={() => this.leave()}
                                                    className={
                                                        hovered.map(hover_data =>
                                                            hover_data.i === i
                                                            && hover_data.j === j
                                                            && hover_data.production === production
                                                            ? "active" : ""
                                                        ).join(" ")
                                                    }
                                                >
                                                    {production} : {data.rule_index}
                                                </li>
                                            )
                                        )}
                                    </ul>
                                </td>
                            )}
                        </tr>
                    )}
                </tbody>
                </table>
            </div>
        );
    }
};

export default Matrice;
