import React from "react";
import ReactDOM from "react-dom";

import Application from "./Application";

const domContainer = document.querySelector('#root');
ReactDOM.render(<Application />, domContainer);

