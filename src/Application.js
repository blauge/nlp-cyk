import React from "react";
import ReactDOM from "react-dom";

import Arbre from "./Arbre.js";
import Matrice from "./Matrice.js";

import cyk from "./cyk.js";
import tokenize from "./tokenize.js";

const onglets = Object.freeze({
    arbre: "arbre",
    matrice: "matrice",
});

const phrases = [
    "Le bien qu'il fait, il le fait bien.",
    "Le fait est que j'ai peu de biens.",
    "J'ai bien du mal à faire ce que les autres font bien.",
    "Bien des gens ont fait un peu de bien.",
    "Bien qu'il ait fait du bien, il ne l'a pas bien fait.",
];

class Application extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            phrase: "",
            grammaire: [],
            onglet: onglets.arbre,
        };
    }

    componentDidMount()
    {
        fetch("./data/grammaire.json")
            .then(res => res.json())
            .then(grammaire =>
            {
                this.setState({
                    grammaire,
                });
            });
    }

    render()
    {
        const {phrase, grammaire, onglet} = this.state;
        const mots = tokenize(phrase);
        const matrice = cyk(mots, grammaire);

        return (
            <div className="application">
                <div className="saisie">
                    <h1>Algorithme CYK</h1>
                    <p className="sous-titre">
                        <a href="https://fr.wikipedia.org/wiki/Algorithme_de_Cocke-Younger-Kasami">
                            Cocke–Younger–Kasami
                        </a>
                    </p>

                    <h2>Saisie</h2>
                    <textarea
                        type="text"
                        placeholder="Saisissez une phrase"
                        value={phrase}
                        onChange={e => this.setState({ phrase: e.target.value })}
                    />

                    <h2>Exemples</h2>

                    <ul className="exemples">
                        {phrases.map((phrase, index) =>
                            <li
                                key={index}
                                onClick={() => this.setState({ phrase })}>
                                {phrase}
                            </li>
                        )}
                    </ul>

                    <h2>À propos</h2>

                    <p>
                        {"Application réalisée par "}
                        <a href="https://fr.linkedin.com/in/remiceres">Rémi Cérès</a>
                        {" ("}
                        <a href="mailto:remiceres@msn.com">email personnel</a>
                        {", "}
                        <a href="mailto:remi.ceres@etu.umontpellier.fr">email universitaire</a>
                        {") et "}
                        <a href="https://matteo.delab.re">Mattéo Delabre</a>
                        {" dans le cadre du cours de "}
                        <em>traitement automatique de la langue naturelle</em>
                        {" à l’université de Montpellier."}
                    </p>
                </div>

                <div className="résultat">
                    <ul className="onglets">
                        <li>
                            <span
                                className={onglet === onglets.arbre ? "actif" : ""}
                                onClick={() => this.setState({onglet: onglets.arbre})}>
                                Arbre
                            </span>
                        </li>
                        <li>
                            <span
                                className={onglet === onglets.matrice ? "actif" : ""}
                                onClick={() => this.setState({onglet: onglets.matrice})}>
                                Matrice
                            </span>
                        </li>
                    </ul>

                    <div className={`affichage ${onglet}`}>
                        {onglet === onglets.arbre ?
                            <Arbre
                                mots={mots}
                                matrice={matrice}
                            /> :
                            <Matrice
                                mots={mots}
                                matrice={matrice}
                                grammaire={grammaire}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}
export default Application;
