const tokenize = phrase =>
{
    if (phrase === "")
    {
        return [];
    }

    // Ignore la majuscule initiale
    phrase = phrase[0].toLowerCase() + phrase.slice(1);

    const result = [];
    let startWord = 0, current = 0;
    const size = phrase.length;

    const add = () => {
        if (startWord < current)
        {
            result.push(phrase.substring(startWord, current));
        }
    };

    while (current < size)
    {
        switch (phrase[current])
        {
        case "'":
        case "’":
            result.push(phrase.substring(startWord, current) + "'");
            ++current;
            startWord = current;
            break;

        case ",":
        case ".":
        case "!":
        case "?":
            add();
            result.push(phrase[current]);
            ++current;
            startWord = current;
            break;

        case " ":
        case "\t":
        case " ":
        case "\n":
            add();
            ++current;
            startWord = current;
            break;

        default:
            ++current;
        }
    }

    add();
    return result;
};

export default tokenize;
